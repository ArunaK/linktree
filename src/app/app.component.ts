import {Component, OnInit} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { User } from "./interface/link-list.interface";
import {MockServiceService} from "./mock-service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  fakeDataProvider!: User;
  title: string = "Linktree";

  constructor(private mockService: MockServiceService) {
  }

  ngOnInit() {
    this.getMockData();

  }

  getMockData() {
    this.mockService.getMockData().subscribe((data) => {
        this.fakeDataProvider = data;
    });
  }

  closeOther(value:boolean) :void{
    //Todo to make sure other list buttong are closed
  }
}

