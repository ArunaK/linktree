import { Injectable } from '@angular/core';
import {User} from "./interface/link-list.interface";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MockServiceService {

  constructor(private http: HttpClient) { }

  getMockData() {
    return this.http.get<User>('./assets/mock/link-list.json');
  }
}
