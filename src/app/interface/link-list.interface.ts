export interface User {
  profile: Profile;
  linkList: Array<LinkList> | Array<MusicLinkList> | Array<ShowLinkList>;
}

export interface Profile {
  profilePictureLink: string;
  profileName?: string;
  "profileSetting": ProfileSetting;
}

export interface ProfileSetting {
  "profilePrimaryColor": string;
  "profileHoverColor": string;
}

export interface LinkList {
  "linkType": string;
  "linkText": string;
  "linkUrl"?: string;
}

export interface MusicLinkList {
  "linkType": string;
  "linkText": string;
  "musicLinkSetting"?: MusicLinkSettings;
}

export interface ShowLinkList {
  "linkType": string;
  "linkText": string;
  "showLinkList"?: ShowLinkSettings;
}

export interface MusicLinkSettings {
  "player": {
    "songName": string;
    "artist": string;
    "thumbnail": string;
  },
  "mediaServiceProvider": Array<mediaServiceProvider>;
}

export interface mediaServiceProvider {
  "mediaServiceName": string;
  "mediaIcon": string;
}

export interface ShowLinkSettings {
  venueProvider: Array<venueProvider>;
}

export interface venueProvider {
  "venueProviderName": string;
  "venueTime": string;
  "venueSoldOut": boolean;
}
