import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MusicLinkComponent } from './components/music-link/music-link.component';
import { ShowLinkComponent } from './components/show-link/show-link.component';
import { ClassicLinkComponent } from './components/classic-link/classic-link.component';
import { ProfileComponent } from './components/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    MusicLinkComponent,
    ShowLinkComponent,
    ClassicLinkComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
