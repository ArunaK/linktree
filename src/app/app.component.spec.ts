import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {User} from "./interface/link-list.interface";
import {of} from "rxjs";
import {MockServiceService} from "./mock-service.service";

describe('AppComponent', () => {

  let mockServiceService: jasmine.SpyObj<MockServiceService>;

  beforeEach(async () => {
    mockServiceService = jasmine.createSpyObj<MockServiceService>([
      'getMockData',
    ]);
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent
      ],
      providers: [{ provide: MockServiceService, useValue: mockServiceService }],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'LinkTree'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Linktree');
  });

  const response: User= {
    "profile": {
      "profilePictureLink": "assets/profile-picture.png",
      "profileName": "test",
      "profileSetting": {
        "profilePrimaryColor": "rebeccapurple",
        "profileHoverColor":"palegoldenrod"
      }
    },
    "linkList": [
      {
        "linkType": "classic",
        "linkText": "classic",
        "linkUrl": "https://www.google.com"
      }
    ]
  };

  it('should call getMockData', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    mockServiceService.getMockData.and.returnValue(of(response));
    fixture.detectChanges();
    fixture.isStable();
    console.log(app.fakeDataProvider);
    expect(app.fakeDataProvider).toBeTruthy();
  });
});
