import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicLinkComponent } from './music-link.component';
import {AppComponent} from "../../app.component";
import {MusicLinkList} from "../../interface/link-list.interface";

describe('MusicLinkComponent', () => {
  let component: MusicLinkComponent;
  let fixture: ComponentFixture<MusicLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicLinkComponent);
    component = fixture.componentInstance;
  });

  const musicList : MusicLinkList = {
    "linkType": "music",
    "linkText": "music",
    "musicLinkSetting": {
    "player": {
      "songName":  "Song Name",
        "artist": "Artist Name",
        "thumbnail": "assets/thumbnail.png"
    },
    "mediaServiceProvider": [
      {
        "mediaServiceName": "Spotify",
        "mediaIcon": "assets/icons/spotify.svg"
      },
      {
        "mediaServiceName": "Apple Music",
        "mediaIcon": "assets/icons/apple-music.svg"
      },
      {
        "mediaServiceName": "Soundcloud",
        "mediaIcon": "assets/icons/soundcloud.svg"
      },
      {
        "mediaServiceName": "YouTube Music",
        "mediaIcon": "assets/icons/youtube.svg"
      },
      {
        "mediaServiceName": "Deezer",
        "mediaIcon": "assets/icons/deezer.svg"
      },
      {
        "mediaServiceName": "Tidal",
        "mediaIcon": "assets/icons/tidal.svg"
      },
      {
        "mediaServiceName": "Bandcamp",
        "mediaIcon": "assets/icons/bandcamp.svg"
      }

    ]
  }
  };

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`showList should be true when button clicked`, () => {
    component.link = musicList;
    fixture.detectChanges();
    component.togglePlayer();
    expect(component.showList).toBeTruthy();
  });
});
