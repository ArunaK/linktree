import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MusicLinkList} from "../../interface/link-list.interface";

@Component({
  selector: 'app-music-link',
  templateUrl: './music-link.component.html',
  styleUrls: ['./music-link.component.scss']
})
export class MusicLinkComponent {

  @Input()
  link!: MusicLinkList;

  @Output() showMusic = new EventEmitter<boolean>();

  showList: boolean = false;

  togglePlayer(){
   this.showList ? this.showList = false : this.showList = true;
    //TODO Event Emitter for the opening and closing of button
    this.showMusic.emit(this.showList);
  }


}
