import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowLinkComponent } from './show-link.component';
import {ShowLinkList} from "../../interface/link-list.interface";

describe('ShowLinkComponent', () => {
  let component: ShowLinkComponent;
  let fixture: ComponentFixture<ShowLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowLinkComponent);
    component = fixture.componentInstance;
  });

  const showList: ShowLinkList = {
    "linkType": "show",
    "linkText": "show",
    "showLinkList": {
      "venueProvider": [
        {
          "venueProviderName": "Venue Name, Canberra",
          "venueTime":"Apr 01 2019",
          "venueSoldOut": false
        },
        {
          "venueProviderName": "Venue Name, Melbourne",
          "venueTime":"Apr 02 2019",
          "venueSoldOut": true
        },
        {
          "venueProviderName": "Venue Name, Sydney",
          "venueTime":"Apr 03 2019",
          "venueSoldOut": false
        },
        {
          "venueProviderName": "Venue Name, Brisbane",
          "venueTime":"Apr 04 2019",
          "venueSoldOut": false
        }
      ]
    }
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`showList should be true when button clicked`, () => {
    component.link = showList;
    fixture.detectChanges();
    component.togglePlayer();
    expect(component.showVenue).toBeTruthy();
  });
});
