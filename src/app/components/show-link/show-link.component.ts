import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ShowLinkList} from "../../interface/link-list.interface";

@Component({
  selector: 'app-show-link',
  templateUrl: './show-link.component.html',
  styleUrls: ['./show-link.component.scss']
})
export class ShowLinkComponent {

  @Input()
  link!: ShowLinkList;

  @Output() showShows = new EventEmitter<boolean>();

  showVenue: boolean = false;

  toggleVenue(){
    this.showVenue ? this.showVenue = false : this.showVenue = true;
    //TODO Event Emitter for the opening and closing of button
    this.showShows.emit(this.showVenue);
  }
}
