import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import {Profile, User} from "../../interface/link-list.interface";

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileComponent ]
    })
    .compileComponents();
  });

  const Profile : Profile = {
    "profilePictureLink": "assets/profile-picture.png",
    "profileName": "test",
    "profileSetting": {
    "profilePrimaryColor": "rebeccapurple",
      "profileHoverColor":"palegoldenrod"
  }
  }

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    component.profile = Profile;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
