import {Component, Input, OnInit} from '@angular/core';
import {LinkList, ProfileSetting} from "../../interface/link-list.interface";

@Component({
  selector: 'app-classic-link',
  templateUrl: './classic-link.component.html',
  styleUrls: ['./classic-link.component.scss']
})
export class ClassicLinkComponent implements OnInit{

  @Input()
  link!: LinkList;

  @Input()
  profileSetting!: ProfileSetting;

  changeColor: boolean = false;
  backgroundColor!: string ;
  fontColor!: string ;

  ngOnInit(){
    this.backgroundColor = this.profileSetting?.profilePrimaryColor;
    this.fontColor = this.profileSetting?.profileHoverColor;
  }

  changeBackgroundColor(hover: boolean) {
    if (hover) {
      this.changeColor = true;
      this.backgroundColor = this.profileSetting?.profileHoverColor;
      this.fontColor =  this.profileSetting?.profilePrimaryColor;
    } else {
      this.changeColor = false;
      this.backgroundColor = this.profileSetting?.profilePrimaryColor;
      this.fontColor =  this.profileSetting?.profileHoverColor;
    }
  }

}
