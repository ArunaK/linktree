import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassicLinkComponent } from './classic-link.component';
import {LinkList} from "../../interface/link-list.interface";

describe('ClassicLinkComponent', () => {
  let component: ClassicLinkComponent;
  let fixture: ComponentFixture<ClassicLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassicLinkComponent ]
    })
    .compileComponents();
  });

  const Link: LinkList = {
    "linkType": "classic",
    "linkText": "classic",
    "linkUrl": "https://www.google.com"
  }

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicLinkComponent);
    component = fixture.componentInstance;
    component.link = Link;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
