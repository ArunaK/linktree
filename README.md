# Code challenge

This Task is for a Littlepay, displays a list of traveller trip data and allows a user to filter the trips by a minimum and maximum fare amount.

## Prerequisites

You will need nodejs and npm to run the project.

## Install dev

Run `npm install`

## Run project in local

Run `ng serve`
Then open [http://localhost:4200/](http://localhost:4200/)

## Running unit tests

I used Karma to unit tests. Most of the code has been covered.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


## Still to be done list 

- Alternate Opening and Closing of the music and show hasn't been done.But the Event Emission has been done 
  But parent component  at this stage is doing nothing with the value 
- Using profile setting to add profile colour for music and show button
- Styling clean up
- Create a component for the icon to fill SVG in the desired colour
- Adding Player
- Styling isn't pixel perfect so still needs a round of improvement
